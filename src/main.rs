#![recursion_limit = "1024"]

extern crate gdk_pixbuf;
extern crate gtk;

#[macro_use]
extern crate error_chain;

use gdk_pixbuf::Pixbuf;
use gtk::prelude::*;
use gtk::{Image, Window, WindowType, WindowExt};

mod error;
mod game;

use error::*;

fn run() -> Result<()>
{
    println!("Starting CrazyMazy…");

    gtk::init()
        .or_else(|_| {
            Err(error::ErrorKind::GtkInit("Unable to initialize GTK".to_string()))
                .into()
        })?;
    
    let window = Window::new(WindowType::Toplevel);

    window.set_title("CrazyMazy");

    let treasurespath = "graphics/treasures";

    let pixbuf = Pixbuf::new_from_file_at_size(
        &format!("{}/key.svg", treasurespath), 70, 70).to_system_result()?;

    let image = Image::new_from_pixbuf(&pixbuf);

    window.add(&image);

    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    gtk::main();

    Ok(())
}

fn main() {
    match run() {
        Err(s) => println!("{}", s),
        Ok(_) => {},
    }
}

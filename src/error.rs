use std;
use gtk;

#[derive(Debug)]
pub struct GtkError {
    description: String,
}

impl std::fmt::Display for GtkError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result
    {
        write!(f, "Gtk error: {}", self.description)
    }
}

impl std::error::Error for GtkError {
    fn description(&self) -> &str {
        &self.description
    }
}

impl std::convert::From<gtk::Error> for GtkError {
    fn from(e: gtk::Error) -> GtkError {
        GtkError {
            description: format!("{}", e),
        }
    }
}

pub trait ToSystemResult<T>
{
    fn to_system_result(self)
        -> std::result::Result<T, Error>;
}

impl<T> ToSystemResult<T> for std::result::Result<T, gtk::Error>
{
    fn to_system_result(self)
        -> std::result::Result<T, Error>
    {
        match self {
            Ok(v) => Ok(v),
            Err(e) => Err(GtkError::from(e).into()),
        }
    }
}

error_chain!{
    foreign_links {
        Gtk(GtkError);
    }
    errors {
        GtkInit(msg: String) {
            display("{}", msg)
        }
    }
}
